from pprint import pprint
from urllib.parse import urljoin

import requests
from django.conf import settings

from django_monstack_icinga2.django_monstack_icinga2.models_icinga2_director import *


def get_or_create_host(
    pseudo_hostname: str,
    merge_data: dict = (),
    skip_cmdb_id=False,
    object_type="object",
):
    object_name_str = pseudo_hostname.lower()
    host = False

    if skip_cmdb_id:
        host_search = object_name_str
    else:
        # pseudo_hostname example: h21-webserver.example.com
        object_cmdb_id = object_name_str.split("-")[0][1:]
        host_search = "h" + object_cmdb_id + "-"

    session = requests.Session()
    session.headers = {
        "Accept": "application/json",
    }

    # Host Object anlegen
    session.auth = (
        settings.ICINGA_DIRECTOR_API_USER,
        settings.ICINGA_DIRECTOR_API_PASSWD,
    )

    try:
        if object_type == "template":
            host = IcingaHost.objects.using("icinga2director").get(
                object_name=host_search, object_type=object_type
            )
        else:
            host = IcingaHost.objects.using("icinga2director").get(
                object_name__startswith=host_search, object_type=object_type
            )
    except IcingaHost.DoesNotExist:
        request_url = urljoin(settings.ICINGA_DIRECTOR_API_URL, "director/host")
    else:
        request_url = urljoin(
            settings.ICINGA_DIRECTOR_API_URL, "director/host?name=" + host.object_name
        )

    edit_host = dict()
    edit_host["accept_config"] = True

    if object_type == "object":
        edit_host["address"] = "127.0.0.1"
        edit_host["address6"] = "::1"
        edit_host["imports"] = ("sl-new-host",)

    edit_host["display_name"] = object_name_str
    edit_host["has_agent"] = False

    edit_host["master_should_connect"] = False
    edit_host["object_name"] = object_name_str
    edit_host["object_type"] = object_type
    edit_host["vars"] = dict()
    edit_host["vars"]["check_up_down"] = True
    edit_host["vars"]["notify_enabled"] = True

    edit_host = {**edit_host, **merge_data}

    request_args = {"url": request_url, "json": edit_host, "verify": False}

    response = session.post(**request_args)

    if response.status_code in (201, 200, 304):
        if object_type == "template":
            host = IcingaHost.objects.using("icinga2director").get(
                object_name=host_search, object_type=object_type
            )
        else:
            host = IcingaHost.objects.using("icinga2director").get(
                object_name__startswith=host_search, object_type=object_type
            )

    return host


def delete_host(object_name: str):
    object_name_str = object_name.lower()

    session = requests.Session()
    session.headers = {
        "Accept": "application/json",
    }

    # Host Object anlegen
    session.auth = (
        settings.ICINGA_DIRECTOR_API_USER,
        settings.ICINGA_DIRECTOR_API_PASSWD,
    )

    request_url = urljoin(
        settings.ICINGA_DIRECTOR_API_URL, "director/host?name=" + object_name_str
    )

    request_args = {"url": request_url, "verify": False}

    response = session.delete(**request_args)

    return True


def deploy():
    # Check if config has been changed
    latest_activity = (
        DirectorActivityLog.objects.using("icinga2director").all().order_by("-id")[0]
    )
    latest_deployment = (
        DirectorDeploymentLog.objects.using("icinga2director").all().order_by("-id")[0]
    )

    if latest_deployment.start_time > latest_activity.change_time:
        return False

    # Rollout new config
    session = requests.Session()
    session.headers = {
        "Accept": "application/json",
    }

    session.auth = (
        settings.ICINGA_DIRECTOR_API_USER,
        settings.ICINGA_DIRECTOR_API_PASSWD,
    )

    request_url = urljoin(settings.ICINGA_DIRECTOR_API_URL, "director/config/deploy")

    request_args = {"url": request_url, "verify": False}

    response = session.post(**request_args)

    return True


def get_or_create_external_command(check_command: str = "dummy"):
    session = requests.Session()
    session.headers = {
        "Accept": "application/json",
    }

    # External Command anlegen
    session.auth = (
        settings.ICINGA_DIRECTOR_API_USER,
        settings.ICINGA_DIRECTOR_API_PASSWD,
    )

    try:
        external_command = IcingaCommand.objects.using("icinga2director").get(
            object_name=check_command
        )
    except IcingaCommand.DoesNotExist:
        request_url = urljoin(settings.ICINGA_DIRECTOR_API_URL, "director/command")
    else:
        request_url = urljoin(
            settings.ICINGA_DIRECTOR_API_URL, "director/command?name=" + check_command
        )

    request_args = {
        "url": request_url,
        # 'json': edit_host,
        "verify": False,
    }

    response = session.post(**request_args)

    pprint(response.text)

    external_command = IcingaCommand.objects.using("icinga2director").get(
        object_name=check_command
    )

    return external_command
