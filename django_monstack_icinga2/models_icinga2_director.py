# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class DirectorActivityLog(models.Model):
    id = models.BigAutoField(primary_key=True)
    object_type = models.CharField(max_length=64)
    object_name = models.CharField(max_length=255)
    action_name = models.CharField(max_length=6)
    old_properties = models.TextField(blank=True, null=True)
    new_properties = models.TextField(blank=True, null=True)
    author = models.CharField(max_length=64)
    change_time = models.DateTimeField()
    checksum = models.CharField(max_length=20)
    parent_checksum = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "director_activity_log"


class DirectorDatafield(models.Model):
    varname = models.CharField(max_length=64)
    caption = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    datatype = models.CharField(max_length=255)
    format = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "director_datafield"


class DirectorDatafieldSetting(models.Model):
    datafield = models.ForeignKey(
        DirectorDatafield, models.DO_NOTHING, related_name="+", primary_key=True
    )
    setting_name = models.CharField(max_length=64)
    setting_value = models.TextField()

    class Meta:
        managed = False
        db_table = "director_datafield_setting"
        unique_together = (("datafield", "setting_name"),)


class DirectorDatalist(models.Model):
    list_name = models.CharField(unique=True, max_length=255)
    owner = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = "director_datalist"


class DirectorDatalistEntry(models.Model):
    list = models.ForeignKey(
        DirectorDatalist, models.DO_NOTHING, related_name="+", primary_key=True
    )
    entry_name = models.CharField(max_length=255)
    entry_value = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=10, blank=True, null=True)
    allowed_roles = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "director_datalist_entry"
        unique_together = (("list", "entry_name"),)


class DirectorDeploymentLog(models.Model):
    id = models.BigAutoField(primary_key=True)
    config_checksum = models.ForeignKey(
        "DirectorGeneratedConfig",
        models.DO_NOTHING,
        related_name="+",
        db_column="config_checksum",
        blank=True,
        null=True,
    )
    last_activity_checksum = models.CharField(max_length=20)
    peer_identity = models.CharField(max_length=64)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(blank=True, null=True)
    abort_time = models.DateTimeField(blank=True, null=True)
    duration_connection = models.PositiveIntegerField(blank=True, null=True)
    duration_dump = models.PositiveIntegerField(blank=True, null=True)
    stage_name = models.CharField(max_length=96, blank=True, null=True)
    stage_collected = models.CharField(max_length=1, blank=True, null=True)
    connection_succeeded = models.CharField(max_length=1, blank=True, null=True)
    dump_succeeded = models.CharField(max_length=1, blank=True, null=True)
    startup_succeeded = models.CharField(max_length=1, blank=True, null=True)
    username = models.CharField(max_length=64, blank=True, null=True)
    startup_log = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "director_deployment_log"


class DirectorGeneratedConfig(models.Model):
    checksum = models.CharField(primary_key=True, max_length=20)
    director_version = models.CharField(max_length=64, blank=True, null=True)
    director_db_version = models.IntegerField(blank=True, null=True)
    duration = models.PositiveIntegerField(blank=True, null=True)
    first_activity_checksum = models.CharField(max_length=20)
    last_activity_checksum = models.ForeignKey(
        DirectorActivityLog,
        models.DO_NOTHING,
        related_name="+",
        db_column="last_activity_checksum",
    )

    class Meta:
        managed = False
        db_table = "director_generated_config"


class DirectorGeneratedConfigFile(models.Model):
    config_checksum = models.ForeignKey(
        DirectorGeneratedConfig,
        models.DO_NOTHING,
        related_name="+",
        db_column="config_checksum",
        primary_key=True,
    )
    file_checksum = models.ForeignKey(
        "DirectorGeneratedFile",
        models.DO_NOTHING,
        related_name="+",
        db_column="file_checksum",
    )
    file_path = models.CharField(max_length=128)

    class Meta:
        managed = False
        db_table = "director_generated_config_file"
        unique_together = (("config_checksum", "file_path"),)


class DirectorGeneratedFile(models.Model):
    checksum = models.CharField(primary_key=True, max_length=20)
    content = models.TextField()
    cnt_object = models.PositiveIntegerField()
    cnt_template = models.PositiveIntegerField()
    cnt_apply = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = "director_generated_file"


class DirectorJob(models.Model):
    job_name = models.CharField(unique=True, max_length=64)
    job_class = models.CharField(max_length=72)
    disabled = models.CharField(max_length=1)
    run_interval = models.PositiveIntegerField()
    timeperiod = models.ForeignKey(
        "IcingaTimeperiod", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    last_attempt_succeeded = models.CharField(max_length=1, blank=True, null=True)
    ts_last_attempt = models.DateTimeField(blank=True, null=True)
    ts_last_error = models.DateTimeField(blank=True, null=True)
    last_error_message = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "director_job"


class DirectorJobSetting(models.Model):
    job = models.ForeignKey(
        DirectorJob, models.DO_NOTHING, related_name="+", primary_key=True
    )
    setting_name = models.CharField(max_length=64)
    setting_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "director_job_setting"
        unique_together = (("job", "setting_name"),)


class DirectorSchemaMigration(models.Model):
    schema_version = models.PositiveSmallIntegerField(primary_key=True)
    migration_time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "director_schema_migration"


class DirectorSetting(models.Model):
    setting_name = models.CharField(primary_key=True, max_length=64)
    setting_value = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = "director_setting"


class IcingaApiuser(models.Model):
    object_name = models.CharField(max_length=255)
    object_type = models.CharField(max_length=15)
    disabled = models.CharField(max_length=1)
    password = models.CharField(max_length=255, blank=True, null=True)
    client_dn = models.CharField(max_length=64, blank=True, null=True)
    permissions = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_apiuser"


class IcingaCommand(models.Model):
    object_name = models.CharField(max_length=255)
    object_type = models.CharField(max_length=15)
    disabled = models.CharField(max_length=1)
    methods_execute = models.CharField(max_length=64, blank=True, null=True)
    command = models.TextField(blank=True, null=True)
    timeout = models.PositiveSmallIntegerField(blank=True, null=True)
    zone = models.ForeignKey(
        "IcingaZone", models.DO_NOTHING, related_name="+", blank=True, null=True
    )

    class Meta:
        managed = False
        db_table = "icinga_command"
        unique_together = (("object_name", "zone"),)


class IcingaCommandArgument(models.Model):
    command = models.ForeignKey(IcingaCommand, models.DO_NOTHING)
    argument_name = models.CharField(max_length=64)
    argument_value = models.TextField(blank=True, null=True)
    argument_format = models.CharField(max_length=10, blank=True, null=True)
    key_string = models.CharField(max_length=64, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    skip_key = models.CharField(max_length=1, blank=True, null=True)
    set_if = models.CharField(max_length=255, blank=True, null=True)
    set_if_format = models.CharField(max_length=10, blank=True, null=True)
    sort_order = models.SmallIntegerField(blank=True, null=True)
    repeat_key = models.CharField(max_length=1, blank=True, null=True)
    required = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_command_argument"
        unique_together = (("command", "argument_name"),)


class IcingaCommandField(models.Model):
    command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", primary_key=True
    )
    datafield = models.ForeignKey(DirectorDatafield, models.DO_NOTHING)
    is_required = models.CharField(max_length=1)
    var_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_command_field"
        unique_together = (("command", "datafield"),)


class IcingaCommandInheritance(models.Model):
    command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_command = models.ForeignKey(IcingaCommand, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_command_inheritance"
        unique_together = (
            ("command", "parent_command"),
            ("command", "weight"),
        )


class IcingaCommandResolvedVar(models.Model):
    command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    checksum = models.ForeignKey(
        "IcingaVar", models.DO_NOTHING, related_name="+", db_column="checksum"
    )

    class Meta:
        managed = False
        db_table = "icinga_command_resolved_var"
        unique_together = (("command", "checksum"),)


class IcingaCommandVar(models.Model):
    command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    varvalue = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=10)
    checksum = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_command_var"
        unique_together = (("command", "varname"),)


class IcingaDependency(models.Model):
    object_name = models.CharField(max_length=255, blank=True, null=True)
    object_type = models.CharField(max_length=8)
    disabled = models.CharField(max_length=1)
    apply_to = models.CharField(max_length=7, blank=True, null=True)
    parent_host = models.ForeignKey(
        "IcingaHost", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    parent_service = models.ForeignKey(
        "IcingaService", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    child_host = models.ForeignKey(
        "IcingaHost", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    child_service = models.ForeignKey(
        "IcingaService", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    disable_checks = models.CharField(max_length=1, blank=True, null=True)
    disable_notifications = models.CharField(max_length=1, blank=True, null=True)
    ignore_soft_states = models.CharField(max_length=1, blank=True, null=True)
    period = models.ForeignKey(
        "IcingaTimeperiod", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    zone = models.ForeignKey(
        "IcingaZone", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    assign_filter = models.TextField(blank=True, null=True)
    parent_service_by_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_dependency"


class IcingaDependencyInheritance(models.Model):
    dependency = models.ForeignKey(
        IcingaDependency, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_dependency = models.ForeignKey(IcingaDependency, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_dependency_inheritance"
        unique_together = (
            ("dependency", "parent_dependency"),
            ("dependency", "weight"),
        )


class IcingaDependencyStatesSet(models.Model):
    dependency = models.ForeignKey(
        IcingaDependency, models.DO_NOTHING, related_name="+", primary_key=True
    )
    property = models.CharField(max_length=8)
    merge_behaviour = models.CharField(max_length=9)

    class Meta:
        managed = False
        db_table = "icinga_dependency_states_set"
        unique_together = (("dependency", "property", "merge_behaviour"),)


class IcingaEndpoint(models.Model):
    zone = models.ForeignKey(
        "IcingaZone", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    object_name = models.CharField(unique=True, max_length=255)
    object_type = models.CharField(max_length=15)
    disabled = models.CharField(max_length=1)
    host = models.CharField(max_length=255, blank=True, null=True)
    port = models.PositiveSmallIntegerField(blank=True, null=True)
    log_duration = models.CharField(max_length=32, blank=True, null=True)
    apiuser = models.ForeignKey(
        IcingaApiuser, models.DO_NOTHING, related_name="+", blank=True, null=True
    )

    class Meta:
        managed = False
        db_table = "icinga_endpoint"


class IcingaEndpointInheritance(models.Model):
    endpoint = models.ForeignKey(
        IcingaEndpoint, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_endpoint = models.ForeignKey(IcingaEndpoint, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_endpoint_inheritance"
        unique_together = (
            ("endpoint", "parent_endpoint"),
            ("endpoint", "weight"),
        )


class IcingaFlatVar(models.Model):
    var_checksum = models.ForeignKey(
        "IcingaVar",
        models.DO_NOTHING,
        related_name="+",
        db_column="var_checksum",
        primary_key=True,
    )
    flatname_checksum = models.CharField(max_length=20)
    flatname = models.CharField(max_length=512)
    flatvalue = models.TextField()

    class Meta:
        managed = False
        db_table = "icinga_flat_var"
        unique_together = (("var_checksum", "flatname_checksum"),)


class IcingaHost(models.Model):
    object_name = models.CharField(unique=True, max_length=255)
    object_type = models.CharField(max_length=8)
    disabled = models.CharField(max_length=1)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=64, blank=True, null=True)
    address6 = models.CharField(max_length=45, blank=True, null=True)
    check_command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    max_check_attempts = models.PositiveIntegerField(blank=True, null=True)
    check_period = models.ForeignKey(
        "IcingaTimeperiod", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    check_interval = models.CharField(max_length=8, blank=True, null=True)
    retry_interval = models.CharField(max_length=8, blank=True, null=True)
    check_timeout = models.PositiveSmallIntegerField(blank=True, null=True)
    enable_notifications = models.CharField(max_length=1, blank=True, null=True)
    enable_active_checks = models.CharField(max_length=1, blank=True, null=True)
    enable_passive_checks = models.CharField(max_length=1, blank=True, null=True)
    enable_event_handler = models.CharField(max_length=1, blank=True, null=True)
    enable_flapping = models.CharField(max_length=1, blank=True, null=True)
    enable_perfdata = models.CharField(max_length=1, blank=True, null=True)
    event_command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    volatile = models.CharField(max_length=1, blank=True, null=True)
    zone = models.ForeignKey(
        "IcingaZone", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    command_endpoint = models.ForeignKey(
        IcingaEndpoint, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    notes = models.TextField(blank=True, null=True)
    notes_url = models.CharField(max_length=255, blank=True, null=True)
    action_url = models.CharField(max_length=255, blank=True, null=True)
    icon_image = models.CharField(max_length=255, blank=True, null=True)
    icon_image_alt = models.CharField(max_length=255, blank=True, null=True)
    has_agent = models.CharField(max_length=1, blank=True, null=True)
    master_should_connect = models.CharField(max_length=1, blank=True, null=True)
    accept_config = models.CharField(max_length=1, blank=True, null=True)
    api_key = models.CharField(unique=True, max_length=40, blank=True, null=True)
    template_choice = models.ForeignKey(
        "IcingaHostTemplateChoice",
        models.DO_NOTHING,
        related_name="+",
        blank=True,
        null=True,
    )
    flapping_threshold_high = models.PositiveSmallIntegerField(blank=True, null=True)
    flapping_threshold_low = models.PositiveSmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_host"


class IcingaHostField(models.Model):
    host = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", primary_key=True
    )
    datafield = models.ForeignKey(DirectorDatafield, models.DO_NOTHING)
    is_required = models.CharField(max_length=1)
    var_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_host_field"
        unique_together = (("host", "datafield"),)


class IcingaHostInheritance(models.Model):
    host = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_host = models.ForeignKey(IcingaHost, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_host_inheritance"
        unique_together = (
            ("host", "parent_host"),
            ("host", "weight"),
        )


class IcingaHostResolvedVar(models.Model):
    host = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    checksum = models.ForeignKey(
        "IcingaVar", models.DO_NOTHING, related_name="+", db_column="checksum"
    )

    class Meta:
        managed = False
        db_table = "icinga_host_resolved_var"
        unique_together = (("host", "checksum"),)


class IcingaHostService(models.Model):
    host = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", primary_key=True
    )
    service = models.ForeignKey("IcingaService", models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_host_service"
        unique_together = (("host", "service"),)


class IcingaHostTemplateChoice(models.Model):
    object_name = models.CharField(unique=True, max_length=64)
    description = models.TextField(blank=True, null=True)
    min_required = models.PositiveSmallIntegerField()
    max_allowed = models.PositiveSmallIntegerField()
    required_template = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    allowed_roles = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_host_template_choice"


class IcingaHostVar(models.Model):
    host = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    varvalue = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=10, blank=True, null=True)
    checksum = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_host_var"
        unique_together = (("host", "varname"),)


class IcingaHostgroup(models.Model):
    object_name = models.CharField(unique=True, max_length=255)
    object_type = models.CharField(max_length=15)
    disabled = models.CharField(max_length=1)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    assign_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostgroup"


class IcingaHostgroupHost(models.Model):
    hostgroup = models.ForeignKey(
        IcingaHostgroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    host = models.ForeignKey(IcingaHost, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_hostgroup_host"
        unique_together = (("hostgroup", "host"),)


class IcingaHostgroupHostResolved(models.Model):
    hostgroup = models.ForeignKey(
        IcingaHostgroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    host = models.ForeignKey(IcingaHost, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_hostgroup_host_resolved"
        unique_together = (("hostgroup", "host"),)


class IcingaHostgroupInheritance(models.Model):
    hostgroup = models.ForeignKey(
        IcingaHostgroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_hostgroup = models.ForeignKey(IcingaHostgroup, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostgroup_inheritance"
        unique_together = (
            ("hostgroup", "parent_hostgroup"),
            ("hostgroup", "weight"),
        )


class IcingaHostgroupParent(models.Model):
    hostgroup = models.ForeignKey(
        IcingaHostgroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_hostgroup = models.ForeignKey(IcingaHostgroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_hostgroup_parent"
        unique_together = (("hostgroup", "parent_hostgroup"),)


class IcingaNotification(models.Model):
    object_name = models.CharField(max_length=255, blank=True, null=True)
    object_type = models.CharField(max_length=8)
    disabled = models.CharField(max_length=1)
    apply_to = models.CharField(max_length=7, blank=True, null=True)
    host = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    service = models.ForeignKey(
        "IcingaService", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    times_begin = models.PositiveIntegerField(blank=True, null=True)
    times_end = models.PositiveIntegerField(blank=True, null=True)
    notification_interval = models.PositiveIntegerField(blank=True, null=True)
    command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    period = models.ForeignKey(
        "IcingaTimeperiod", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    zone = models.ForeignKey(
        "IcingaZone", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    assign_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_notification"


class IcingaNotificationField(models.Model):
    notification = models.ForeignKey(
        IcingaNotification, models.DO_NOTHING, related_name="+", primary_key=True
    )
    datafield = models.ForeignKey(DirectorDatafield, models.DO_NOTHING)
    is_required = models.CharField(max_length=1)
    var_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_notification_field"
        unique_together = (("notification", "datafield"),)


class IcingaNotificationInheritance(models.Model):
    notification = models.ForeignKey(
        IcingaNotification, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_notification = models.ForeignKey(IcingaNotification, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_notification_inheritance"
        unique_together = (
            ("notification", "parent_notification"),
            ("notification", "weight"),
        )


class IcingaNotificationResolvedVar(models.Model):
    notification = models.ForeignKey(
        IcingaNotification, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    checksum = models.ForeignKey(
        "IcingaVar", models.DO_NOTHING, related_name="+", db_column="checksum"
    )

    class Meta:
        managed = False
        db_table = "icinga_notification_resolved_var"
        unique_together = (("notification", "checksum"),)


class IcingaNotificationStatesSet(models.Model):
    notification = models.ForeignKey(
        IcingaNotification, models.DO_NOTHING, related_name="+", primary_key=True
    )
    property = models.CharField(max_length=8)
    merge_behaviour = models.CharField(max_length=9)

    class Meta:
        managed = False
        db_table = "icinga_notification_states_set"
        unique_together = (("notification", "property", "merge_behaviour"),)


class IcingaNotificationTypesSet(models.Model):
    notification = models.ForeignKey(
        IcingaNotification, models.DO_NOTHING, related_name="+", primary_key=True
    )
    property = models.CharField(max_length=15)
    merge_behaviour = models.CharField(max_length=9)

    class Meta:
        managed = False
        db_table = "icinga_notification_types_set"
        unique_together = (("notification", "property", "merge_behaviour"),)


class IcingaNotificationUser(models.Model):
    notification = models.ForeignKey(
        IcingaNotification, models.DO_NOTHING, related_name="+", primary_key=True
    )
    user = models.ForeignKey("IcingaUser", models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_notification_user"
        unique_together = (("notification", "user"),)


class IcingaNotificationUsergroup(models.Model):
    notification = models.ForeignKey(
        IcingaNotification, models.DO_NOTHING, related_name="+", primary_key=True
    )
    usergroup = models.ForeignKey("IcingaUsergroup", models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_notification_usergroup"
        unique_together = (("notification", "usergroup"),)


class IcingaNotificationVar(models.Model):
    notification = models.ForeignKey(
        IcingaNotification, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    varvalue = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=10, blank=True, null=True)
    checksum = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_notification_var"
        unique_together = (("notification", "varname"),)


class IcingaService(models.Model):
    object_name = models.CharField(max_length=255)
    object_type = models.CharField(max_length=8)
    disabled = models.CharField(max_length=1)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    host = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    service_set = models.ForeignKey(
        "IcingaServiceSet", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    check_command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    max_check_attempts = models.PositiveIntegerField(blank=True, null=True)
    check_period = models.ForeignKey(
        "IcingaTimeperiod", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    check_interval = models.CharField(max_length=8, blank=True, null=True)
    retry_interval = models.CharField(max_length=8, blank=True, null=True)
    check_timeout = models.PositiveSmallIntegerField(blank=True, null=True)
    enable_notifications = models.CharField(max_length=1, blank=True, null=True)
    enable_active_checks = models.CharField(max_length=1, blank=True, null=True)
    enable_passive_checks = models.CharField(max_length=1, blank=True, null=True)
    enable_event_handler = models.CharField(max_length=1, blank=True, null=True)
    enable_flapping = models.CharField(max_length=1, blank=True, null=True)
    enable_perfdata = models.CharField(max_length=1, blank=True, null=True)
    event_command = models.ForeignKey(
        IcingaCommand, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    volatile = models.CharField(max_length=1, blank=True, null=True)
    zone = models.ForeignKey(
        "IcingaZone", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    command_endpoint = models.ForeignKey(
        IcingaEndpoint, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    notes = models.TextField(blank=True, null=True)
    notes_url = models.CharField(max_length=255, blank=True, null=True)
    action_url = models.CharField(max_length=255, blank=True, null=True)
    icon_image = models.CharField(max_length=255, blank=True, null=True)
    icon_image_alt = models.CharField(max_length=255, blank=True, null=True)
    use_agent = models.CharField(max_length=1, blank=True, null=True)
    apply_for = models.CharField(max_length=255, blank=True, null=True)
    use_var_overrides = models.CharField(max_length=1, blank=True, null=True)
    assign_filter = models.TextField(blank=True, null=True)
    template_choice = models.ForeignKey(
        "IcingaServiceTemplateChoice",
        models.DO_NOTHING,
        related_name="+",
        blank=True,
        null=True,
    )
    flapping_threshold_high = models.PositiveSmallIntegerField(blank=True, null=True)
    flapping_threshold_low = models.PositiveSmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service"
        unique_together = (("object_name", "host"),)


class IcingaServiceField(models.Model):
    service = models.ForeignKey(
        IcingaService, models.DO_NOTHING, related_name="+", primary_key=True
    )
    datafield = models.ForeignKey(DirectorDatafield, models.DO_NOTHING)
    is_required = models.CharField(max_length=1)
    var_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_field"
        unique_together = (("service", "datafield"),)


class IcingaServiceInheritance(models.Model):
    service = models.ForeignKey(
        IcingaService, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_service = models.ForeignKey(IcingaService, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_inheritance"
        unique_together = (
            ("service", "parent_service"),
            ("service", "weight"),
        )


class IcingaServiceResolvedVar(models.Model):
    service = models.ForeignKey(
        IcingaService, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    checksum = models.ForeignKey(
        "IcingaVar", models.DO_NOTHING, related_name="+", db_column="checksum"
    )

    class Meta:
        managed = False
        db_table = "icinga_service_resolved_var"
        unique_together = (("service", "checksum"),)


class IcingaServiceSet(models.Model):
    object_name = models.CharField(max_length=128)
    object_type = models.CharField(max_length=15)
    host = models.ForeignKey(
        IcingaHost, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    description = models.TextField(blank=True, null=True)
    assign_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_set"
        unique_together = (("object_name", "host"),)


class IcingaServiceSetInheritance(models.Model):
    service_set = models.ForeignKey(
        IcingaServiceSet, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_service_set = models.ForeignKey(IcingaServiceSet, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_set_inheritance"
        unique_together = (
            ("service_set", "parent_service_set"),
            ("service_set", "weight"),
        )


class IcingaServiceSetResolvedVar(models.Model):
    service_set = models.ForeignKey(
        IcingaServiceSet, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    checksum = models.ForeignKey(
        "IcingaVar", models.DO_NOTHING, related_name="+", db_column="checksum"
    )

    class Meta:
        managed = False
        db_table = "icinga_service_set_resolved_var"
        unique_together = (("service_set", "checksum"),)


class IcingaServiceSetVar(models.Model):
    service_set = models.ForeignKey(
        IcingaServiceSet, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    varvalue = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=10)
    checksum = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_set_var"
        unique_together = (("service_set", "varname"),)


class IcingaServiceTemplateChoice(models.Model):
    object_name = models.CharField(unique=True, max_length=64)
    description = models.TextField(blank=True, null=True)
    min_required = models.PositiveSmallIntegerField()
    max_allowed = models.PositiveSmallIntegerField()
    required_template = models.ForeignKey(
        IcingaService, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    allowed_roles = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_template_choice"


class IcingaServiceVar(models.Model):
    service = models.ForeignKey(
        IcingaService, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    varvalue = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=10, blank=True, null=True)
    checksum = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_var"
        unique_together = (("service", "varname"),)


class IcingaServicegroup(models.Model):
    object_name = models.CharField(unique=True, max_length=255, blank=True, null=True)
    object_type = models.CharField(max_length=8)
    disabled = models.CharField(max_length=1)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    assign_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_servicegroup"


class IcingaServicegroupInheritance(models.Model):
    servicegroup = models.ForeignKey(
        IcingaServicegroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_servicegroup = models.ForeignKey(IcingaServicegroup, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_servicegroup_inheritance"
        unique_together = (
            ("servicegroup", "parent_servicegroup"),
            ("servicegroup", "weight"),
        )


class IcingaServicegroupService(models.Model):
    servicegroup = models.ForeignKey(
        IcingaServicegroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    service = models.ForeignKey(IcingaService, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_servicegroup_service"
        unique_together = (("servicegroup", "service"),)


class IcingaTimeperiod(models.Model):
    object_name = models.CharField(max_length=255)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    update_method = models.CharField(max_length=64, blank=True, null=True)
    zone = models.ForeignKey(
        "IcingaZone", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    object_type = models.CharField(max_length=8)
    disabled = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = "icinga_timeperiod"
        unique_together = (("object_name", "zone"),)


class IcingaTimeperiodInheritance(models.Model):
    timeperiod = models.ForeignKey(
        IcingaTimeperiod, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_timeperiod = models.ForeignKey(IcingaTimeperiod, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_timeperiod_inheritance"
        unique_together = (
            ("timeperiod", "parent_timeperiod"),
            ("timeperiod", "weight"),
        )


class IcingaTimeperiodRange(models.Model):
    timeperiod = models.ForeignKey(
        IcingaTimeperiod, models.DO_NOTHING, related_name="+", primary_key=True
    )
    range_key = models.CharField(max_length=255)
    range_value = models.CharField(max_length=255)
    range_type = models.CharField(max_length=7)
    merge_behaviour = models.CharField(max_length=9)

    class Meta:
        managed = False
        db_table = "icinga_timeperiod_range"
        unique_together = (("timeperiod", "range_type", "range_key"),)


class IcingaUser(models.Model):
    object_name = models.CharField(max_length=255, blank=True, null=True)
    object_type = models.CharField(max_length=8)
    disabled = models.CharField(max_length=1)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    pager = models.CharField(max_length=255, blank=True, null=True)
    enable_notifications = models.CharField(max_length=1, blank=True, null=True)
    period_id = models.PositiveIntegerField(blank=True, null=True)
    zone = models.ForeignKey(
        "IcingaZone", models.DO_NOTHING, related_name="+", blank=True, null=True
    )

    class Meta:
        managed = False
        db_table = "icinga_user"
        unique_together = (("object_name", "zone"),)


class IcingaUserField(models.Model):
    user = models.ForeignKey(
        IcingaUser, models.DO_NOTHING, related_name="+", primary_key=True
    )
    datafield = models.ForeignKey(DirectorDatafield, models.DO_NOTHING)
    is_required = models.CharField(max_length=1)
    var_filter = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_user_field"
        unique_together = (("user", "datafield"),)


class IcingaUserInheritance(models.Model):
    user = models.ForeignKey(
        IcingaUser, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_user = models.ForeignKey(IcingaUser, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_user_inheritance"
        unique_together = (
            ("user", "parent_user"),
            ("user", "weight"),
        )


class IcingaUserResolvedVar(models.Model):
    user = models.ForeignKey(
        IcingaUser, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    checksum = models.ForeignKey(
        "IcingaVar", models.DO_NOTHING, related_name="+", db_column="checksum"
    )

    class Meta:
        managed = False
        db_table = "icinga_user_resolved_var"
        unique_together = (("user", "checksum"),)


class IcingaUserStatesSet(models.Model):
    user = models.ForeignKey(
        IcingaUser, models.DO_NOTHING, related_name="+", primary_key=True
    )
    property = models.CharField(max_length=8)
    merge_behaviour = models.CharField(max_length=9)

    class Meta:
        managed = False
        db_table = "icinga_user_states_set"
        unique_together = (("user", "property", "merge_behaviour"),)


class IcingaUserTypesSet(models.Model):
    user = models.ForeignKey(
        IcingaUser, models.DO_NOTHING, related_name="+", primary_key=True
    )
    property = models.CharField(max_length=15)
    merge_behaviour = models.CharField(max_length=9)

    class Meta:
        managed = False
        db_table = "icinga_user_types_set"
        unique_together = (("user", "property", "merge_behaviour"),)


class IcingaUserVar(models.Model):
    user = models.ForeignKey(
        IcingaUser, models.DO_NOTHING, related_name="+", primary_key=True
    )
    varname = models.CharField(max_length=255)
    varvalue = models.TextField(blank=True, null=True)
    format = models.CharField(max_length=10)
    checksum = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_user_var"
        unique_together = (("user", "varname"),)


class IcingaUsergroup(models.Model):
    object_name = models.CharField(unique=True, max_length=255)
    object_type = models.CharField(max_length=8)
    disabled = models.CharField(max_length=1)
    display_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_usergroup"


class IcingaUsergroupInheritance(models.Model):
    usergroup = models.ForeignKey(
        IcingaUsergroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_usergroup = models.ForeignKey(IcingaUsergroup, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_usergroup_inheritance"
        unique_together = (
            ("usergroup", "parent_usergroup"),
            ("usergroup", "weight"),
        )


class IcingaUsergroupParent(models.Model):
    usergroup = models.ForeignKey(
        IcingaUsergroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_usergroup = models.ForeignKey(IcingaUsergroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_usergroup_parent"
        unique_together = (("usergroup", "parent_usergroup"),)


class IcingaUsergroupUser(models.Model):
    usergroup = models.ForeignKey(
        IcingaUsergroup, models.DO_NOTHING, related_name="+", primary_key=True
    )
    user = models.ForeignKey(IcingaUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "icinga_usergroup_user"
        unique_together = (("usergroup", "user"),)


class IcingaVar(models.Model):
    checksum = models.CharField(primary_key=True, max_length=20)
    rendered_checksum = models.CharField(max_length=20)
    varname = models.CharField(max_length=255)
    varvalue = models.TextField()
    rendered = models.TextField()

    class Meta:
        managed = False
        db_table = "icinga_var"


class IcingaZone(models.Model):
    parent = models.ForeignKey(
        "self", models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    object_name = models.CharField(unique=True, max_length=255)
    object_type = models.CharField(max_length=15)
    disabled = models.CharField(max_length=1)
    is_global = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = "icinga_zone"


class IcingaZoneInheritance(models.Model):
    zone = models.ForeignKey(
        IcingaZone, models.DO_NOTHING, related_name="+", primary_key=True
    )
    parent_zone = models.ForeignKey(IcingaZone, models.DO_NOTHING)
    weight = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_zone_inheritance"
        unique_together = (
            ("zone", "parent_zone"),
            ("zone", "weight"),
        )


class ImportRowModifier(models.Model):
    source = models.ForeignKey("ImportSource", models.DO_NOTHING)
    property_name = models.CharField(max_length=255)
    target_property = models.CharField(max_length=255, blank=True, null=True)
    provider_class = models.CharField(max_length=72)
    priority = models.PositiveSmallIntegerField()
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "import_row_modifier"


class ImportRowModifierSetting(models.Model):
    row_modifier = models.ForeignKey(
        ImportRowModifier, models.DO_NOTHING, related_name="+", primary_key=True
    )
    setting_name = models.CharField(max_length=64)
    setting_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "import_row_modifier_setting"
        unique_together = (("row_modifier", "setting_name"),)


class ImportRun(models.Model):
    source = models.ForeignKey("ImportSource", models.DO_NOTHING)
    rowset_checksum = models.ForeignKey(
        "ImportedRowset",
        models.DO_NOTHING,
        related_name="+",
        db_column="rowset_checksum",
        blank=True,
        null=True,
    )
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(blank=True, null=True)
    succeeded = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "import_run"


class ImportSource(models.Model):
    source_name = models.CharField(max_length=64)
    key_column = models.CharField(max_length=64)
    provider_class = models.CharField(max_length=72)
    import_state = models.CharField(max_length=15)
    last_error_message = models.TextField(blank=True, null=True)
    last_attempt = models.DateTimeField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "import_source"


class ImportSourceSetting(models.Model):
    source = models.ForeignKey(
        ImportSource, models.DO_NOTHING, related_name="+", primary_key=True
    )
    setting_name = models.CharField(max_length=64)
    setting_value = models.TextField()

    class Meta:
        managed = False
        db_table = "import_source_setting"
        unique_together = (("source", "setting_name"),)


class ImportedProperty(models.Model):
    checksum = models.CharField(primary_key=True, max_length=20)
    property_name = models.CharField(max_length=64)
    property_value = models.TextField()
    format = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "imported_property"


class ImportedRow(models.Model):
    checksum = models.CharField(primary_key=True, max_length=20)
    object_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = "imported_row"


class ImportedRowProperty(models.Model):
    row_checksum = models.ForeignKey(
        ImportedRow,
        models.DO_NOTHING,
        related_name="+",
        db_column="row_checksum",
        primary_key=True,
    )
    property_checksum = models.ForeignKey(
        ImportedProperty,
        models.DO_NOTHING,
        related_name="+",
        db_column="property_checksum",
    )

    class Meta:
        managed = False
        db_table = "imported_row_property"
        unique_together = (("row_checksum", "property_checksum"),)


class ImportedRowset(models.Model):
    checksum = models.CharField(primary_key=True, max_length=20)

    class Meta:
        managed = False
        db_table = "imported_rowset"


class ImportedRowsetRow(models.Model):
    rowset_checksum = models.ForeignKey(
        ImportedRowset,
        models.DO_NOTHING,
        related_name="+",
        db_column="rowset_checksum",
        primary_key=True,
    )
    row_checksum = models.ForeignKey(
        ImportedRow, models.DO_NOTHING, related_name="+", db_column="row_checksum"
    )

    class Meta:
        managed = False
        db_table = "imported_rowset_row"
        unique_together = (("rowset_checksum", "row_checksum"),)


class SyncProperty(models.Model):
    rule = models.ForeignKey("SyncRule", models.DO_NOTHING)
    source = models.ForeignKey(ImportSource, models.DO_NOTHING)
    source_expression = models.CharField(max_length=255)
    destination_field = models.CharField(max_length=64, blank=True, null=True)
    priority = models.PositiveSmallIntegerField()
    filter_expression = models.TextField(blank=True, null=True)
    merge_policy = models.CharField(max_length=8)

    class Meta:
        managed = False
        db_table = "sync_property"


class SyncRule(models.Model):
    rule_name = models.CharField(max_length=255)
    object_type = models.CharField(max_length=13)
    update_policy = models.CharField(max_length=8)
    purge_existing = models.CharField(max_length=1)
    filter_expression = models.TextField(blank=True, null=True)
    sync_state = models.CharField(max_length=15)
    last_error_message = models.TextField(blank=True, null=True)
    last_attempt = models.DateTimeField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "sync_rule"


class SyncRun(models.Model):
    id = models.BigAutoField(primary_key=True)
    rule = models.ForeignKey(
        SyncRule, models.DO_NOTHING, related_name="+", blank=True, null=True
    )
    rule_name = models.CharField(max_length=255)
    start_time = models.DateTimeField()
    duration_ms = models.PositiveIntegerField(blank=True, null=True)
    objects_deleted = models.PositiveIntegerField(blank=True, null=True)
    objects_created = models.PositiveIntegerField(blank=True, null=True)
    objects_modified = models.PositiveIntegerField(blank=True, null=True)
    last_former_activity = models.CharField(max_length=20, blank=True, null=True)
    last_related_activity = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "sync_run"
