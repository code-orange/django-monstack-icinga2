# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class CusGroups(models.Model):
    groupid = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=20)
    cust_nr = models.CharField(max_length=20)
    auto_create = models.IntegerField()
    deploy_token = models.CharField(max_length=100)
    deploy_version = models.CharField(max_length=20)
    sync_verinice = models.IntegerField()

    class Meta:
        managed = False
        db_table = "cus_groups"


class IcingaAcknowledgements(models.Model):
    acknowledgement_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    entry_time = models.DateTimeField(blank=True, null=True)
    entry_time_usec = models.IntegerField(blank=True, null=True)
    acknowledgement_type = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    state = models.SmallIntegerField(blank=True, null=True)
    author_name = models.CharField(max_length=64, blank=True, null=True)
    comment_data = models.TextField(blank=True, null=True)
    is_sticky = models.SmallIntegerField(blank=True, null=True)
    persistent_comment = models.SmallIntegerField(blank=True, null=True)
    notify_contacts = models.SmallIntegerField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_acknowledgements"


class IcingaCommands(models.Model):
    command_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    command_line = models.TextField(blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_commands"
        unique_together = (("instance_id", "object_id", "config_type"),)


class IcingaCommenthistory(models.Model):
    commenthistory_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    entry_time = models.DateTimeField(blank=True, null=True)
    entry_time_usec = models.IntegerField(blank=True, null=True)
    comment_type = models.SmallIntegerField(blank=True, null=True)
    entry_type = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    comment_time = models.DateTimeField(blank=True, null=True)
    internal_comment_id = models.BigIntegerField(blank=True, null=True)
    author_name = models.CharField(max_length=64, blank=True, null=True)
    comment_data = models.TextField(blank=True, null=True)
    is_persistent = models.SmallIntegerField(blank=True, null=True)
    comment_source = models.SmallIntegerField(blank=True, null=True)
    expires = models.SmallIntegerField(blank=True, null=True)
    expiration_time = models.DateTimeField(blank=True, null=True)
    deletion_time = models.DateTimeField(blank=True, null=True)
    deletion_time_usec = models.IntegerField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_commenthistory"


class IcingaComments(models.Model):
    comment_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    entry_time = models.DateTimeField(blank=True, null=True)
    entry_time_usec = models.IntegerField(blank=True, null=True)
    comment_type = models.SmallIntegerField(blank=True, null=True)
    entry_type = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    comment_time = models.DateTimeField(blank=True, null=True)
    internal_comment_id = models.BigIntegerField(blank=True, null=True)
    author_name = models.CharField(max_length=64, blank=True, null=True)
    comment_data = models.TextField(blank=True, null=True)
    is_persistent = models.SmallIntegerField(blank=True, null=True)
    comment_source = models.SmallIntegerField(blank=True, null=True)
    expires = models.SmallIntegerField(blank=True, null=True)
    expiration_time = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    session_token = models.IntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_comments"


class IcingaConfigfiles(models.Model):
    configfile_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    configfile_type = models.SmallIntegerField(blank=True, null=True)
    configfile_path = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_configfiles"
        unique_together = (("instance_id", "configfile_type", "configfile_path"),)


class IcingaConfigfilevariables(models.Model):
    configfilevariable_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    configfile_id = models.BigIntegerField(blank=True, null=True)
    varname = models.CharField(max_length=64, blank=True, null=True)
    varvalue = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_configfilevariables"


class IcingaConninfo(models.Model):
    conninfo_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    agent_name = models.CharField(max_length=32, blank=True, null=True)
    agent_version = models.CharField(max_length=32, blank=True, null=True)
    disposition = models.CharField(max_length=32, blank=True, null=True)
    connect_source = models.CharField(max_length=32, blank=True, null=True)
    connect_type = models.CharField(max_length=32, blank=True, null=True)
    connect_time = models.DateTimeField(blank=True, null=True)
    disconnect_time = models.DateTimeField(blank=True, null=True)
    last_checkin_time = models.DateTimeField(blank=True, null=True)
    data_start_time = models.DateTimeField(blank=True, null=True)
    data_end_time = models.DateTimeField(blank=True, null=True)
    bytes_processed = models.BigIntegerField(blank=True, null=True)
    lines_processed = models.BigIntegerField(blank=True, null=True)
    entries_processed = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_conninfo"


class IcingaContactAddresses(models.Model):
    contact_address_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    contact_id = models.BigIntegerField(blank=True, null=True)
    address_number = models.SmallIntegerField(blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_contact_addresses"
        unique_together = (("contact_id", "address_number"),)


class IcingaContactNotificationcommands(models.Model):
    contact_notificationcommand_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    contact_id = models.BigIntegerField(blank=True, null=True)
    notification_type = models.SmallIntegerField(blank=True, null=True)
    command_object_id = models.BigIntegerField(blank=True, null=True)
    command_args = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_contact_notificationcommands"
        unique_together = (
            ("contact_id", "notification_type", "command_object_id", "command_args"),
        )


class IcingaContactgroupMembers(models.Model):
    contactgroup_member_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    contactgroup_id = models.BigIntegerField(blank=True, null=True)
    contact_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_contactgroup_members"


class IcingaContactgroups(models.Model):
    contactgroup_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    contactgroup_object_id = models.BigIntegerField(blank=True, null=True)
    alias = models.CharField(max_length=255, blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_contactgroups"
        unique_together = (("instance_id", "config_type", "contactgroup_object_id"),)


class IcingaContactnotificationmethods(models.Model):
    contactnotificationmethod_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    contactnotification_id = models.BigIntegerField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    start_time_usec = models.IntegerField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    end_time_usec = models.IntegerField(blank=True, null=True)
    command_object_id = models.BigIntegerField(blank=True, null=True)
    command_args = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_contactnotificationmethods"
        unique_together = (
            ("instance_id", "contactnotification_id", "start_time", "start_time_usec"),
        )


class IcingaContactnotifications(models.Model):
    contactnotification_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    notification_id = models.BigIntegerField(blank=True, null=True)
    contact_object_id = models.BigIntegerField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    start_time_usec = models.IntegerField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    end_time_usec = models.IntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_contactnotifications"
        unique_together = (
            ("instance_id", "contact_object_id", "start_time", "start_time_usec"),
        )


class IcingaContacts(models.Model):
    contact_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    contact_object_id = models.BigIntegerField(blank=True, null=True)
    alias = models.CharField(max_length=255, blank=True, null=True)
    email_address = models.CharField(max_length=255, blank=True, null=True)
    pager_address = models.CharField(max_length=64, blank=True, null=True)
    host_timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    service_timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    host_notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    service_notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    can_submit_commands = models.SmallIntegerField(blank=True, null=True)
    notify_service_recovery = models.SmallIntegerField(blank=True, null=True)
    notify_service_warning = models.SmallIntegerField(blank=True, null=True)
    notify_service_unknown = models.SmallIntegerField(blank=True, null=True)
    notify_service_critical = models.SmallIntegerField(blank=True, null=True)
    notify_service_flapping = models.SmallIntegerField(blank=True, null=True)
    notify_service_downtime = models.SmallIntegerField(blank=True, null=True)
    notify_host_recovery = models.SmallIntegerField(blank=True, null=True)
    notify_host_down = models.SmallIntegerField(blank=True, null=True)
    notify_host_unreachable = models.SmallIntegerField(blank=True, null=True)
    notify_host_flapping = models.SmallIntegerField(blank=True, null=True)
    notify_host_downtime = models.SmallIntegerField(blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_contacts"
        unique_together = (("instance_id", "config_type", "contact_object_id"),)


class IcingaContactstatus(models.Model):
    contactstatus_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    contact_object_id = models.BigIntegerField(unique=True, blank=True, null=True)
    status_update_time = models.DateTimeField(blank=True, null=True)
    host_notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    service_notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    last_host_notification = models.DateTimeField(blank=True, null=True)
    last_service_notification = models.DateTimeField(blank=True, null=True)
    modified_attributes = models.IntegerField(blank=True, null=True)
    modified_host_attributes = models.IntegerField(blank=True, null=True)
    modified_service_attributes = models.IntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_contactstatus"


class IcingaCustomvariables(models.Model):
    customvariable_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    has_been_modified = models.SmallIntegerField(blank=True, null=True)
    varname = models.CharField(max_length=255, blank=True, null=True)
    varvalue = models.TextField(blank=True, null=True)
    is_json = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_customvariables"
        unique_together = (("object_id", "config_type", "varname"),)


class IcingaCustomvariablestatus(models.Model):
    customvariablestatus_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    status_update_time = models.DateTimeField(blank=True, null=True)
    has_been_modified = models.SmallIntegerField(blank=True, null=True)
    varname = models.CharField(max_length=255, blank=True, null=True)
    varvalue = models.TextField(blank=True, null=True)
    is_json = models.SmallIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_customvariablestatus"
        unique_together = (("object_id", "varname"),)


class IcingaDbversion(models.Model):
    dbversion_id = models.BigAutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=10, blank=True, null=True)
    version = models.CharField(max_length=10, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    modify_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_dbversion"


class IcingaDowntimehistory(models.Model):
    downtimehistory_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    downtime_type = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    entry_time = models.DateTimeField(blank=True, null=True)
    author_name = models.CharField(max_length=64, blank=True, null=True)
    comment_data = models.TextField(blank=True, null=True)
    internal_downtime_id = models.BigIntegerField(blank=True, null=True)
    triggered_by_id = models.BigIntegerField(blank=True, null=True)
    is_fixed = models.SmallIntegerField(blank=True, null=True)
    duration = models.BigIntegerField(blank=True, null=True)
    scheduled_start_time = models.DateTimeField(blank=True, null=True)
    scheduled_end_time = models.DateTimeField(blank=True, null=True)
    was_started = models.SmallIntegerField(blank=True, null=True)
    actual_start_time = models.DateTimeField(blank=True, null=True)
    actual_start_time_usec = models.IntegerField(blank=True, null=True)
    actual_end_time = models.DateTimeField(blank=True, null=True)
    actual_end_time_usec = models.IntegerField(blank=True, null=True)
    was_cancelled = models.SmallIntegerField(blank=True, null=True)
    is_in_effect = models.SmallIntegerField(blank=True, null=True)
    trigger_time = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_downtimehistory"


class IcingaEndpoints(models.Model):
    endpoint_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)
    zone_object_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    identity = models.CharField(max_length=255, blank=True, null=True)
    node = models.CharField(max_length=255, blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_endpoints"


class IcingaEndpointstatus(models.Model):
    endpointstatus_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)
    zone_object_id = models.BigIntegerField(blank=True, null=True)
    status_update_time = models.DateTimeField(blank=True, null=True)
    identity = models.CharField(max_length=255, blank=True, null=True)
    node = models.CharField(max_length=255, blank=True, null=True)
    is_connected = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_endpointstatus"


class IcingaEventhandlers(models.Model):
    eventhandler_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    eventhandler_type = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    state = models.SmallIntegerField(blank=True, null=True)
    state_type = models.SmallIntegerField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    start_time_usec = models.IntegerField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    end_time_usec = models.IntegerField(blank=True, null=True)
    command_object_id = models.BigIntegerField(blank=True, null=True)
    command_args = models.TextField(blank=True, null=True)
    command_line = models.TextField(blank=True, null=True)
    timeout = models.SmallIntegerField(blank=True, null=True)
    early_timeout = models.SmallIntegerField(blank=True, null=True)
    execution_time = models.FloatField(blank=True, null=True)
    return_code = models.SmallIntegerField(blank=True, null=True)
    output = models.TextField(blank=True, null=True)
    long_output = models.TextField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_eventhandlers"
        unique_together = (
            ("instance_id", "object_id", "start_time", "start_time_usec"),
        )


class IcingaExternalcommands(models.Model):
    externalcommand_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    entry_time = models.DateTimeField(blank=True, null=True)
    command_type = models.SmallIntegerField(blank=True, null=True)
    command_name = models.CharField(max_length=128, blank=True, null=True)
    command_args = models.TextField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_externalcommands"


class IcingaFlappinghistory(models.Model):
    flappinghistory_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    event_time = models.DateTimeField(blank=True, null=True)
    event_time_usec = models.IntegerField(blank=True, null=True)
    event_type = models.SmallIntegerField(blank=True, null=True)
    reason_type = models.SmallIntegerField(blank=True, null=True)
    flapping_type = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    percent_state_change = models.FloatField(blank=True, null=True)
    low_threshold = models.FloatField(blank=True, null=True)
    high_threshold = models.FloatField(blank=True, null=True)
    comment_time = models.DateTimeField(blank=True, null=True)
    internal_comment_id = models.BigIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_flappinghistory"


class IcingaHostContactgroups(models.Model):
    host_contactgroup_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    host_id = models.BigIntegerField(blank=True, null=True)
    contactgroup_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_host_contactgroups"


class IcingaHostContacts(models.Model):
    host_contact_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    host_id = models.BigIntegerField(blank=True, null=True)
    contact_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_host_contacts"


class IcingaHostParenthosts(models.Model):
    host_parenthost_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    host_id = models.BigIntegerField(blank=True, null=True)
    parent_host_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_host_parenthosts"


class IcingaHostchecks(models.Model):
    hostcheck_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    host_object_id = models.BigIntegerField(blank=True, null=True)
    check_type = models.SmallIntegerField(blank=True, null=True)
    is_raw_check = models.SmallIntegerField(blank=True, null=True)
    current_check_attempt = models.SmallIntegerField(blank=True, null=True)
    max_check_attempts = models.SmallIntegerField(blank=True, null=True)
    state = models.SmallIntegerField(blank=True, null=True)
    state_type = models.SmallIntegerField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    start_time_usec = models.IntegerField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    end_time_usec = models.IntegerField(blank=True, null=True)
    command_object_id = models.BigIntegerField(blank=True, null=True)
    command_args = models.TextField(blank=True, null=True)
    command_line = models.TextField(blank=True, null=True)
    timeout = models.SmallIntegerField(blank=True, null=True)
    early_timeout = models.SmallIntegerField(blank=True, null=True)
    execution_time = models.FloatField(blank=True, null=True)
    latency = models.FloatField(blank=True, null=True)
    return_code = models.SmallIntegerField(blank=True, null=True)
    output = models.TextField(blank=True, null=True)
    long_output = models.TextField(blank=True, null=True)
    perfdata = models.TextField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostchecks"


class IcingaHostdependencies(models.Model):
    hostdependency_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    host_object_id = models.BigIntegerField(blank=True, null=True)
    dependent_host_object_id = models.BigIntegerField(blank=True, null=True)
    dependency_type = models.SmallIntegerField(blank=True, null=True)
    inherits_parent = models.SmallIntegerField(blank=True, null=True)
    timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    fail_on_up = models.SmallIntegerField(blank=True, null=True)
    fail_on_down = models.SmallIntegerField(blank=True, null=True)
    fail_on_unreachable = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostdependencies"


class IcingaHostescalationContactgroups(models.Model):
    hostescalation_contactgroup_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    hostescalation_id = models.BigIntegerField(blank=True, null=True)
    contactgroup_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostescalation_contactgroups"
        unique_together = (("hostescalation_id", "contactgroup_object_id"),)


class IcingaHostescalationContacts(models.Model):
    hostescalation_contact_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    hostescalation_id = models.BigIntegerField(blank=True, null=True)
    contact_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostescalation_contacts"
        unique_together = (("instance_id", "hostescalation_id", "contact_object_id"),)


class IcingaHostescalations(models.Model):
    hostescalation_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    host_object_id = models.BigIntegerField(blank=True, null=True)
    timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    first_notification = models.SmallIntegerField(blank=True, null=True)
    last_notification = models.SmallIntegerField(blank=True, null=True)
    notification_interval = models.FloatField(blank=True, null=True)
    escalate_on_recovery = models.SmallIntegerField(blank=True, null=True)
    escalate_on_down = models.SmallIntegerField(blank=True, null=True)
    escalate_on_unreachable = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostescalations"
        unique_together = (
            (
                "instance_id",
                "config_type",
                "host_object_id",
                "timeperiod_object_id",
                "first_notification",
                "last_notification",
            ),
        )


class IcingaHostgroupMembers(models.Model):
    hostgroup_member_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    hostgroup_id = models.BigIntegerField(blank=True, null=True)
    host_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostgroup_members"


class IcingaHostgroups(models.Model):
    hostgroup_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    hostgroup_object_id = models.BigIntegerField(blank=True, null=True)
    alias = models.CharField(max_length=255, blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    notes_url = models.TextField(blank=True, null=True)
    action_url = models.TextField(blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hostgroups"
        unique_together = (("instance_id", "hostgroup_object_id"),)


class IcingaHosts(models.Model):
    host_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    host_object_id = models.BigIntegerField(blank=True, null=True)
    alias = models.CharField(max_length=255, blank=True, null=True)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=128, blank=True, null=True)
    address6 = models.CharField(max_length=128, blank=True, null=True)
    check_command_object_id = models.BigIntegerField(blank=True, null=True)
    check_command_args = models.TextField(blank=True, null=True)
    eventhandler_command_object_id = models.BigIntegerField(blank=True, null=True)
    eventhandler_command_args = models.TextField(blank=True, null=True)
    notification_timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    check_timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    failure_prediction_options = models.CharField(max_length=128, blank=True, null=True)
    check_interval = models.FloatField(blank=True, null=True)
    retry_interval = models.FloatField(blank=True, null=True)
    max_check_attempts = models.SmallIntegerField(blank=True, null=True)
    first_notification_delay = models.FloatField(blank=True, null=True)
    notification_interval = models.FloatField(blank=True, null=True)
    notify_on_down = models.SmallIntegerField(blank=True, null=True)
    notify_on_unreachable = models.SmallIntegerField(blank=True, null=True)
    notify_on_recovery = models.SmallIntegerField(blank=True, null=True)
    notify_on_flapping = models.SmallIntegerField(blank=True, null=True)
    notify_on_downtime = models.SmallIntegerField(blank=True, null=True)
    stalk_on_up = models.SmallIntegerField(blank=True, null=True)
    stalk_on_down = models.SmallIntegerField(blank=True, null=True)
    stalk_on_unreachable = models.SmallIntegerField(blank=True, null=True)
    flap_detection_enabled = models.SmallIntegerField(blank=True, null=True)
    flap_detection_on_up = models.SmallIntegerField(blank=True, null=True)
    flap_detection_on_down = models.SmallIntegerField(blank=True, null=True)
    flap_detection_on_unreachable = models.SmallIntegerField(blank=True, null=True)
    low_flap_threshold = models.FloatField(blank=True, null=True)
    high_flap_threshold = models.FloatField(blank=True, null=True)
    process_performance_data = models.SmallIntegerField(blank=True, null=True)
    freshness_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    freshness_threshold = models.IntegerField(blank=True, null=True)
    passive_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    event_handler_enabled = models.SmallIntegerField(blank=True, null=True)
    active_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    retain_status_information = models.SmallIntegerField(blank=True, null=True)
    retain_nonstatus_information = models.SmallIntegerField(blank=True, null=True)
    notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    obsess_over_host = models.SmallIntegerField(blank=True, null=True)
    failure_prediction_enabled = models.SmallIntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    notes_url = models.TextField(blank=True, null=True)
    action_url = models.TextField(blank=True, null=True)
    icon_image = models.TextField(blank=True, null=True)
    icon_image_alt = models.TextField(blank=True, null=True)
    vrml_image = models.TextField(blank=True, null=True)
    statusmap_image = models.TextField(blank=True, null=True)
    have_2d_coords = models.SmallIntegerField(blank=True, null=True)
    x_2d = models.SmallIntegerField(blank=True, null=True)
    y_2d = models.SmallIntegerField(blank=True, null=True)
    have_3d_coords = models.SmallIntegerField(blank=True, null=True)
    x_3d = models.FloatField(blank=True, null=True)
    y_3d = models.FloatField(blank=True, null=True)
    z_3d = models.FloatField(blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hosts"
        unique_together = (("instance_id", "config_type", "host_object_id"),)


class IcingaHoststatus(models.Model):
    hoststatus_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    host_object_id = models.BigIntegerField(unique=True, blank=True, null=True)
    status_update_time = models.DateTimeField(blank=True, null=True)
    output = models.TextField(blank=True, null=True)
    long_output = models.TextField(blank=True, null=True)
    perfdata = models.TextField(blank=True, null=True)
    check_source = models.CharField(max_length=255, blank=True, null=True)
    current_state = models.SmallIntegerField(blank=True, null=True)
    has_been_checked = models.SmallIntegerField(blank=True, null=True)
    should_be_scheduled = models.SmallIntegerField(blank=True, null=True)
    current_check_attempt = models.SmallIntegerField(blank=True, null=True)
    max_check_attempts = models.SmallIntegerField(blank=True, null=True)
    last_check = models.DateTimeField(blank=True, null=True)
    next_check = models.DateTimeField(blank=True, null=True)
    check_type = models.SmallIntegerField(blank=True, null=True)
    last_state_change = models.DateTimeField(blank=True, null=True)
    last_hard_state_change = models.DateTimeField(blank=True, null=True)
    last_hard_state = models.SmallIntegerField(blank=True, null=True)
    last_time_up = models.DateTimeField(blank=True, null=True)
    last_time_down = models.DateTimeField(blank=True, null=True)
    last_time_unreachable = models.DateTimeField(blank=True, null=True)
    state_type = models.SmallIntegerField(blank=True, null=True)
    last_notification = models.DateTimeField(blank=True, null=True)
    next_notification = models.DateTimeField(blank=True, null=True)
    no_more_notifications = models.SmallIntegerField(blank=True, null=True)
    notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    problem_has_been_acknowledged = models.SmallIntegerField(blank=True, null=True)
    acknowledgement_type = models.SmallIntegerField(blank=True, null=True)
    current_notification_number = models.PositiveIntegerField(blank=True, null=True)
    passive_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    active_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    event_handler_enabled = models.SmallIntegerField(blank=True, null=True)
    flap_detection_enabled = models.SmallIntegerField(blank=True, null=True)
    is_flapping = models.SmallIntegerField(blank=True, null=True)
    percent_state_change = models.FloatField(blank=True, null=True)
    latency = models.FloatField(blank=True, null=True)
    execution_time = models.FloatField(blank=True, null=True)
    scheduled_downtime_depth = models.SmallIntegerField(blank=True, null=True)
    failure_prediction_enabled = models.SmallIntegerField(blank=True, null=True)
    process_performance_data = models.SmallIntegerField(blank=True, null=True)
    obsess_over_host = models.SmallIntegerField(blank=True, null=True)
    modified_host_attributes = models.IntegerField(blank=True, null=True)
    original_attributes = models.TextField(blank=True, null=True)
    event_handler = models.TextField(blank=True, null=True)
    check_command = models.TextField(blank=True, null=True)
    normal_check_interval = models.FloatField(blank=True, null=True)
    retry_check_interval = models.FloatField(blank=True, null=True)
    check_timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    is_reachable = models.SmallIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_hoststatus"


class IcingaInstances(models.Model):
    instance_id = models.BigAutoField(primary_key=True)
    instance_name = models.CharField(max_length=64, blank=True, null=True)
    instance_description = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_instances"


class IcingaLogentries(models.Model):
    logentry_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    logentry_time = models.DateTimeField(blank=True, null=True)
    entry_time = models.DateTimeField(blank=True, null=True)
    entry_time_usec = models.IntegerField(blank=True, null=True)
    logentry_type = models.IntegerField(blank=True, null=True)
    logentry_data = models.TextField(blank=True, null=True)
    realtime_data = models.SmallIntegerField(blank=True, null=True)
    inferred_data_extracted = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_logentries"


class IcingaNotifications(models.Model):
    notification_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    notification_type = models.SmallIntegerField(blank=True, null=True)
    notification_reason = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    start_time_usec = models.IntegerField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    end_time_usec = models.IntegerField(blank=True, null=True)
    state = models.SmallIntegerField(blank=True, null=True)
    output = models.TextField(blank=True, null=True)
    long_output = models.TextField(blank=True, null=True)
    escalated = models.SmallIntegerField(blank=True, null=True)
    contacts_notified = models.SmallIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_notifications"
        unique_together = (
            ("instance_id", "object_id", "start_time", "start_time_usec"),
        )


class IcingaObjects(models.Model):
    object_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    objecttype_id = models.BigIntegerField(blank=True, null=True)
    name1 = models.CharField(max_length=128, blank=True, null=True)
    name2 = models.CharField(max_length=128, blank=True, null=True)
    is_active = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_objects"


class IcingaProcessevents(models.Model):
    processevent_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    event_type = models.SmallIntegerField(blank=True, null=True)
    event_time = models.DateTimeField(blank=True, null=True)
    event_time_usec = models.IntegerField(blank=True, null=True)
    process_id = models.BigIntegerField(blank=True, null=True)
    program_name = models.CharField(max_length=16, blank=True, null=True)
    program_version = models.CharField(max_length=20, blank=True, null=True)
    program_date = models.CharField(max_length=10, blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_processevents"


class IcingaProgramstatus(models.Model):
    programstatus_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(unique=True, blank=True, null=True)
    program_version = models.CharField(max_length=64, blank=True, null=True)
    status_update_time = models.DateTimeField(blank=True, null=True)
    program_start_time = models.DateTimeField(blank=True, null=True)
    program_end_time = models.DateTimeField(blank=True, null=True)
    endpoint_name = models.CharField(max_length=255, blank=True, null=True)
    is_currently_running = models.SmallIntegerField(blank=True, null=True)
    process_id = models.BigIntegerField(blank=True, null=True)
    daemon_mode = models.SmallIntegerField(blank=True, null=True)
    last_command_check = models.DateTimeField(blank=True, null=True)
    last_log_rotation = models.DateTimeField(blank=True, null=True)
    notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    disable_notif_expire_time = models.DateTimeField(blank=True, null=True)
    active_service_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    passive_service_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    active_host_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    passive_host_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    event_handlers_enabled = models.SmallIntegerField(blank=True, null=True)
    flap_detection_enabled = models.SmallIntegerField(blank=True, null=True)
    failure_prediction_enabled = models.SmallIntegerField(blank=True, null=True)
    process_performance_data = models.SmallIntegerField(blank=True, null=True)
    obsess_over_hosts = models.SmallIntegerField(blank=True, null=True)
    obsess_over_services = models.SmallIntegerField(blank=True, null=True)
    modified_host_attributes = models.IntegerField(blank=True, null=True)
    modified_service_attributes = models.IntegerField(blank=True, null=True)
    global_host_event_handler = models.TextField(blank=True, null=True)
    global_service_event_handler = models.TextField(blank=True, null=True)
    config_dump_in_progress = models.SmallIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_programstatus"


class IcingaRuntimevariables(models.Model):
    runtimevariable_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    varname = models.CharField(max_length=64, blank=True, null=True)
    varvalue = models.TextField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_runtimevariables"


class IcingaScheduleddowntime(models.Model):
    scheduleddowntime_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    downtime_type = models.SmallIntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    entry_time = models.DateTimeField(blank=True, null=True)
    author_name = models.CharField(max_length=64, blank=True, null=True)
    comment_data = models.TextField(blank=True, null=True)
    internal_downtime_id = models.BigIntegerField(blank=True, null=True)
    triggered_by_id = models.BigIntegerField(blank=True, null=True)
    is_fixed = models.SmallIntegerField(blank=True, null=True)
    duration = models.BigIntegerField(blank=True, null=True)
    scheduled_start_time = models.DateTimeField(blank=True, null=True)
    scheduled_end_time = models.DateTimeField(blank=True, null=True)
    was_started = models.SmallIntegerField(blank=True, null=True)
    actual_start_time = models.DateTimeField(blank=True, null=True)
    actual_start_time_usec = models.IntegerField(blank=True, null=True)
    is_in_effect = models.SmallIntegerField(blank=True, null=True)
    trigger_time = models.DateTimeField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)
    session_token = models.IntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_scheduleddowntime"


class IcingaServiceContactgroups(models.Model):
    service_contactgroup_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    service_id = models.BigIntegerField(blank=True, null=True)
    contactgroup_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_contactgroups"


class IcingaServiceContacts(models.Model):
    service_contact_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    service_id = models.BigIntegerField(blank=True, null=True)
    contact_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_service_contacts"


class IcingaServicechecks(models.Model):
    servicecheck_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    service_object_id = models.BigIntegerField(blank=True, null=True)
    check_type = models.SmallIntegerField(blank=True, null=True)
    current_check_attempt = models.SmallIntegerField(blank=True, null=True)
    max_check_attempts = models.SmallIntegerField(blank=True, null=True)
    state = models.SmallIntegerField(blank=True, null=True)
    state_type = models.SmallIntegerField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    start_time_usec = models.IntegerField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    end_time_usec = models.IntegerField(blank=True, null=True)
    command_object_id = models.BigIntegerField(blank=True, null=True)
    command_args = models.TextField(blank=True, null=True)
    command_line = models.TextField(blank=True, null=True)
    timeout = models.SmallIntegerField(blank=True, null=True)
    early_timeout = models.SmallIntegerField(blank=True, null=True)
    execution_time = models.FloatField(blank=True, null=True)
    latency = models.FloatField(blank=True, null=True)
    return_code = models.SmallIntegerField(blank=True, null=True)
    output = models.TextField(blank=True, null=True)
    long_output = models.TextField(blank=True, null=True)
    perfdata = models.TextField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_servicechecks"


class IcingaServicedependencies(models.Model):
    servicedependency_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    service_object_id = models.BigIntegerField(blank=True, null=True)
    dependent_service_object_id = models.BigIntegerField(blank=True, null=True)
    dependency_type = models.SmallIntegerField(blank=True, null=True)
    inherits_parent = models.SmallIntegerField(blank=True, null=True)
    timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    fail_on_ok = models.SmallIntegerField(blank=True, null=True)
    fail_on_warning = models.SmallIntegerField(blank=True, null=True)
    fail_on_unknown = models.SmallIntegerField(blank=True, null=True)
    fail_on_critical = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_servicedependencies"


class IcingaServiceescalationContactgroups(models.Model):
    serviceescalation_contactgroup_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    serviceescalation_id = models.BigIntegerField(blank=True, null=True)
    contactgroup_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_serviceescalation_contactgroups"
        unique_together = (("serviceescalation_id", "contactgroup_object_id"),)


class IcingaServiceescalationContacts(models.Model):
    serviceescalation_contact_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    serviceescalation_id = models.BigIntegerField(blank=True, null=True)
    contact_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_serviceescalation_contacts"
        unique_together = (
            ("instance_id", "serviceescalation_id", "contact_object_id"),
        )


class IcingaServiceescalations(models.Model):
    serviceescalation_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    service_object_id = models.BigIntegerField(blank=True, null=True)
    timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    first_notification = models.SmallIntegerField(blank=True, null=True)
    last_notification = models.SmallIntegerField(blank=True, null=True)
    notification_interval = models.FloatField(blank=True, null=True)
    escalate_on_recovery = models.SmallIntegerField(blank=True, null=True)
    escalate_on_warning = models.SmallIntegerField(blank=True, null=True)
    escalate_on_unknown = models.SmallIntegerField(blank=True, null=True)
    escalate_on_critical = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_serviceescalations"
        unique_together = (
            (
                "instance_id",
                "config_type",
                "service_object_id",
                "timeperiod_object_id",
                "first_notification",
                "last_notification",
            ),
        )


class IcingaServicegroupMembers(models.Model):
    servicegroup_member_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    servicegroup_id = models.BigIntegerField(blank=True, null=True)
    service_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_servicegroup_members"


class IcingaServicegroups(models.Model):
    servicegroup_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    servicegroup_object_id = models.BigIntegerField(blank=True, null=True)
    alias = models.CharField(max_length=255, blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    notes_url = models.TextField(blank=True, null=True)
    action_url = models.TextField(blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_servicegroups"
        unique_together = (("instance_id", "config_type", "servicegroup_object_id"),)


class IcingaServices(models.Model):
    service_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    host_object_id = models.BigIntegerField(blank=True, null=True)
    service_object_id = models.BigIntegerField(blank=True, null=True)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    check_command_object_id = models.BigIntegerField(blank=True, null=True)
    check_command_args = models.TextField(blank=True, null=True)
    eventhandler_command_object_id = models.BigIntegerField(blank=True, null=True)
    eventhandler_command_args = models.TextField(blank=True, null=True)
    notification_timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    check_timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    failure_prediction_options = models.CharField(max_length=64, blank=True, null=True)
    check_interval = models.FloatField(blank=True, null=True)
    retry_interval = models.FloatField(blank=True, null=True)
    max_check_attempts = models.SmallIntegerField(blank=True, null=True)
    first_notification_delay = models.FloatField(blank=True, null=True)
    notification_interval = models.FloatField(blank=True, null=True)
    notify_on_warning = models.SmallIntegerField(blank=True, null=True)
    notify_on_unknown = models.SmallIntegerField(blank=True, null=True)
    notify_on_critical = models.SmallIntegerField(blank=True, null=True)
    notify_on_recovery = models.SmallIntegerField(blank=True, null=True)
    notify_on_flapping = models.SmallIntegerField(blank=True, null=True)
    notify_on_downtime = models.SmallIntegerField(blank=True, null=True)
    stalk_on_ok = models.SmallIntegerField(blank=True, null=True)
    stalk_on_warning = models.SmallIntegerField(blank=True, null=True)
    stalk_on_unknown = models.SmallIntegerField(blank=True, null=True)
    stalk_on_critical = models.SmallIntegerField(blank=True, null=True)
    is_volatile = models.SmallIntegerField(blank=True, null=True)
    flap_detection_enabled = models.SmallIntegerField(blank=True, null=True)
    flap_detection_on_ok = models.SmallIntegerField(blank=True, null=True)
    flap_detection_on_warning = models.SmallIntegerField(blank=True, null=True)
    flap_detection_on_unknown = models.SmallIntegerField(blank=True, null=True)
    flap_detection_on_critical = models.SmallIntegerField(blank=True, null=True)
    low_flap_threshold = models.FloatField(blank=True, null=True)
    high_flap_threshold = models.FloatField(blank=True, null=True)
    process_performance_data = models.SmallIntegerField(blank=True, null=True)
    freshness_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    freshness_threshold = models.IntegerField(blank=True, null=True)
    passive_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    event_handler_enabled = models.SmallIntegerField(blank=True, null=True)
    active_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    retain_status_information = models.SmallIntegerField(blank=True, null=True)
    retain_nonstatus_information = models.SmallIntegerField(blank=True, null=True)
    notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    obsess_over_service = models.SmallIntegerField(blank=True, null=True)
    failure_prediction_enabled = models.SmallIntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    notes_url = models.TextField(blank=True, null=True)
    action_url = models.TextField(blank=True, null=True)
    icon_image = models.TextField(blank=True, null=True)
    icon_image_alt = models.TextField(blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_services"
        unique_together = (("instance_id", "config_type", "service_object_id"),)


class IcingaServicestatus(models.Model):
    servicestatus_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    service_object_id = models.BigIntegerField(unique=True, blank=True, null=True)
    status_update_time = models.DateTimeField(blank=True, null=True)
    output = models.TextField(blank=True, null=True)
    long_output = models.TextField(blank=True, null=True)
    perfdata = models.TextField(blank=True, null=True)
    check_source = models.CharField(max_length=255, blank=True, null=True)
    current_state = models.SmallIntegerField(blank=True, null=True)
    has_been_checked = models.SmallIntegerField(blank=True, null=True)
    should_be_scheduled = models.SmallIntegerField(blank=True, null=True)
    current_check_attempt = models.SmallIntegerField(blank=True, null=True)
    max_check_attempts = models.SmallIntegerField(blank=True, null=True)
    last_check = models.DateTimeField(blank=True, null=True)
    next_check = models.DateTimeField(blank=True, null=True)
    check_type = models.SmallIntegerField(blank=True, null=True)
    last_state_change = models.DateTimeField(blank=True, null=True)
    last_hard_state_change = models.DateTimeField(blank=True, null=True)
    last_hard_state = models.SmallIntegerField(blank=True, null=True)
    last_time_ok = models.DateTimeField(blank=True, null=True)
    last_time_warning = models.DateTimeField(blank=True, null=True)
    last_time_unknown = models.DateTimeField(blank=True, null=True)
    last_time_critical = models.DateTimeField(blank=True, null=True)
    state_type = models.SmallIntegerField(blank=True, null=True)
    last_notification = models.DateTimeField(blank=True, null=True)
    next_notification = models.DateTimeField(blank=True, null=True)
    no_more_notifications = models.SmallIntegerField(blank=True, null=True)
    notifications_enabled = models.SmallIntegerField(blank=True, null=True)
    problem_has_been_acknowledged = models.SmallIntegerField(blank=True, null=True)
    acknowledgement_type = models.SmallIntegerField(blank=True, null=True)
    current_notification_number = models.PositiveIntegerField(blank=True, null=True)
    passive_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    active_checks_enabled = models.SmallIntegerField(blank=True, null=True)
    event_handler_enabled = models.SmallIntegerField(blank=True, null=True)
    flap_detection_enabled = models.SmallIntegerField(blank=True, null=True)
    is_flapping = models.SmallIntegerField(blank=True, null=True)
    percent_state_change = models.FloatField(blank=True, null=True)
    latency = models.FloatField(blank=True, null=True)
    execution_time = models.FloatField(blank=True, null=True)
    scheduled_downtime_depth = models.SmallIntegerField(blank=True, null=True)
    failure_prediction_enabled = models.SmallIntegerField(blank=True, null=True)
    process_performance_data = models.SmallIntegerField(blank=True, null=True)
    obsess_over_service = models.SmallIntegerField(blank=True, null=True)
    modified_service_attributes = models.IntegerField(blank=True, null=True)
    original_attributes = models.TextField(blank=True, null=True)
    event_handler = models.TextField(blank=True, null=True)
    check_command = models.TextField(blank=True, null=True)
    normal_check_interval = models.FloatField(blank=True, null=True)
    retry_check_interval = models.FloatField(blank=True, null=True)
    check_timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    is_reachable = models.SmallIntegerField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_servicestatus"


class IcingaStatehistory(models.Model):
    statehistory_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    state_time = models.DateTimeField(blank=True, null=True)
    state_time_usec = models.IntegerField(blank=True, null=True)
    object_id = models.BigIntegerField(blank=True, null=True)
    state_change = models.SmallIntegerField(blank=True, null=True)
    state = models.SmallIntegerField(blank=True, null=True)
    state_type = models.SmallIntegerField(blank=True, null=True)
    current_check_attempt = models.SmallIntegerField(blank=True, null=True)
    max_check_attempts = models.SmallIntegerField(blank=True, null=True)
    last_state = models.SmallIntegerField(blank=True, null=True)
    last_hard_state = models.SmallIntegerField(blank=True, null=True)
    output = models.TextField(blank=True, null=True)
    long_output = models.TextField(blank=True, null=True)
    check_source = models.CharField(max_length=255, blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_statehistory"


class IcingaSystemcommands(models.Model):
    systemcommand_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    start_time_usec = models.IntegerField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    end_time_usec = models.IntegerField(blank=True, null=True)
    command_line = models.TextField(blank=True, null=True)
    timeout = models.SmallIntegerField(blank=True, null=True)
    early_timeout = models.SmallIntegerField(blank=True, null=True)
    execution_time = models.FloatField(blank=True, null=True)
    return_code = models.SmallIntegerField(blank=True, null=True)
    output = models.TextField(blank=True, null=True)
    long_output = models.TextField(blank=True, null=True)
    endpoint_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_systemcommands"
        unique_together = (("instance_id", "start_time", "start_time_usec"),)


class IcingaTimeperiodTimeranges(models.Model):
    timeperiod_timerange_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    timeperiod_id = models.BigIntegerField(blank=True, null=True)
    day = models.SmallIntegerField(blank=True, null=True)
    start_sec = models.IntegerField(blank=True, null=True)
    end_sec = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_timeperiod_timeranges"


class IcingaTimeperiods(models.Model):
    timeperiod_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    timeperiod_object_id = models.BigIntegerField(blank=True, null=True)
    alias = models.CharField(max_length=255, blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_timeperiods"
        unique_together = (("instance_id", "config_type", "timeperiod_object_id"),)


class IcingaZones(models.Model):
    zone_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    zone_object_id = models.BigIntegerField(blank=True, null=True)
    config_type = models.SmallIntegerField(blank=True, null=True)
    parent_zone_object_id = models.BigIntegerField(blank=True, null=True)
    is_global = models.SmallIntegerField(blank=True, null=True)
    config_hash = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_zones"


class IcingaZonestatus(models.Model):
    zonestatus_id = models.BigAutoField(primary_key=True)
    instance_id = models.BigIntegerField(blank=True, null=True)
    zone_object_id = models.BigIntegerField(blank=True, null=True)
    status_update_time = models.DateTimeField(blank=True, null=True)
    parent_zone_object_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "icinga_zonestatus"
